import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    params = {"per_page": 1, "querey": city + " " + state}
    url = "https://api.pexels.com/v1/search"
    res = requests.get(url, params=params, headers=headers)
    pexel_dict = res.json()

    try:
        picture_url = pexel_dict["photos"][0]["src"]["original"]
        return {"picture_url": picture_url}
    except KeyError:
        return {"picture_url": None}

    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_lat_lon(city, state):
    url = "https://api.openweathermap.org/geo/10/direct"
    params = {"q": f"{city},{state},USA", "appid": OPEN_WEATHER_API_KEY}
    res = requests.get(url, params=params)
    the_json = res.json()
    lat = the_json[0]["lat"]
    lon = the_json[0]["lon"]
    return lat, lon


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
    lat, lon = get_lat_lon(city, state)
    url = "https://api.openweathermap.org/data/2.5/weather/"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    res = requests.get(url, params=params)
    the_json = res.json()
    return {
        "description": the_json["weather"][0]["description"],
        "temp": the_json["main"]["temp"],
    }
