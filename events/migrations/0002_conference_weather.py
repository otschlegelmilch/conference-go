# Generated by Django 4.0.3 on 2022-09-29 21:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='conference',
            name='weather',
            field=models.TextField(default=1),
            preserve_default=False,
        ),
    ]
