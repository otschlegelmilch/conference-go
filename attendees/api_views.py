from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

from .models import Attendee
from events.models import Conference


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
    ]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        response = []
        attendees = Attendee.objects.all()
        for attendee in attendees:
            response.append(
                {
                    "name": attendee.name,
                    "href": attendee.get_api_url(),
                }
            )
        return JsonResponse({"attendees": response})

    else:
        content = json.loads(request.body)

    # Get the Conference object and put it in the content dict
    try:
        conference = Conference.objects.get(id=conference_id)
        content["conference"] = conference
    except Conference.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
        )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )


def api_show_attendee(request, pk):
    """
    Returns the details for the Attendee model specified
    by the pk parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    attendee = Attendee.objects.get(id=pk)
    return JsonResponse(
        {
            "name": attendee.name,
            "starts": attendee.starts,
            "ends": attendee.ends,
            "description": attendee.description,
            "created": attendee.created,
            "updated": attendee.updated,
            "max_presentations": attendee.max_presentations,
            "max_attendees": attendee.max_attendees,
            "location": {
                "name": attendee.location.name,
                "href": attendee.location.get_api_url(),
            },
        }
    )
